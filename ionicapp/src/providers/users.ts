import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Md5 } from 'ts-md5/dist/md5';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class Users {

  // Límite de registros por página
  perpage: number = 2;


  constructor(public http: Http, private loadingCtrl: LoadingController) {
  }

  //WI-Fi
  server = 'http://localhost:8080';

  registerUser(data) {
    data.password = Md5.hashStr(data.password);
    return this.postPromise({ route: this.server + '/api/users/create', data: data });

      }

  loginUser(data) {

    return this.postPromise({ route: this.server + '/api/users/login', data: { input: data.input, password: Md5.hashStr(data.password) } });
  
  }

  /*Descactivar usuario */

  changeRole(userId, role) {
    return this.putPromise({ route: this.server + '/api/users/changerole/' + userId, data: { role: role } });

  }

  logoutUser(data) {
    localStorage.clear();
  }

  /*-- Roberto --*/

  /*Función para generar contraseña AUTOMÁTICA*/
  newPasswdAuto(data) {
    return this.putPromise({ route: this.server + '/api/users/autopassw', data: data });

  }

  /*Funcion para cambiar la contraseña, comprobamos que el email/dni existe en la base de datos y después le añadimos la nueva contraseña.*/
  newPassword(data) {
    data.password = Md5.hashStr(data.password);
    data.oldpassword = (data.oldpassword) ? Md5.hashStr(data.oldpassword) : null;

    return this.putPromise({ route: this.server + '/api/users/resetpassw', data: data });

  }

  getAllUsers() {
    return this.getPromise({ route: this.server + '/api/users/read' });

  }

  deleteUser(deleteUserId: String) {
    return this.deletePromise({ info: this.server + '/api/users/delete?_id=' + deleteUserId })

  }

  modifyUser(data) {
    data.password = Md5.hashStr(data.password);
    return this.putPromise({ route: this.server + '/api/users/update', data: data });

  }

  //Gestion de vacaciones
  addHollidays(data) {
    console.log(data);
    return this.putPromise({ route: this.server + '/api/users/update', data: data });

  }

  addPersonalDays(data) {
    console.log(data);
    return this.putPromise({ route: this.server + '/api/users/addPersonalDays', data: data });

  }
  updateHollidays(data) {
    return this.putPromise({ route: this.server + '/api/users/updateHollidays', data: data });


  }

  //Fichar
  Check(userId, data) {

    console.log(userId);
    console.log(data);
    return this.putPromise({ route: this.server + '/api/users/check?_id=' + userId, data: data });
  }

  /* Metodo de prueba para la paginación del listado de usuarios */
  load(page: number = 0) {
    return this.getPromise({ route: this.server + '/api/users/readPage?page=' + page });
    
  }

  createLoading(msg) {
    return this.loadingCtrl.create({
      content: msg
    });
  }

  getPromise(info) {
    return new Promise(resolve => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.get(info.route, { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        });
    });
  }

  postPromise(info) {
    return new Promise(resolve => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(info.route, JSON.stringify(info.data), { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
          resolve(data);
        });
    });
  }

  putPromise(info) {
    return new Promise(resolve => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.put(info.route, JSON.stringify(info.data), { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          console.log("service")
          console.log(data);
          resolve(data);
        });
    });
  }

  deletePromise(info) {
    return new Promise(resolve => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.delete(info.route, { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        });
    });
  }

}
